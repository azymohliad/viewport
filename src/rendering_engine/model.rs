use std::path::Path;
use anyhow::{Result, anyhow};

use super::gpu;


pub struct Model {
    pub meshes: Vec<Mesh>,
    pub materials: Vec<Material>,
}


pub struct Mesh {
    pub name: String,
    pub vertices: Vec<gpu::Vertex>,
    pub indices: Vec<u32>,
    pub material_id: Option<usize>,
}

// #[derive(Debug)]
pub struct Material {
    pub name: String,
    pub ambient_color: [f32; 3],
    pub diffuse_color: [f32; 3],
    pub specular_color: [f32; 3],
    pub shininess: f32,
    pub dissolve: f32,
    pub optical_density: f32,
    pub ambient_texture: Option<image::RgbaImage>,
    pub diffuse_texture: Option<image::RgbaImage>,
    pub specular_texture: Option<image::RgbaImage>,
    pub normal_texture: Option<image::RgbaImage>,
    pub dissolve_texture: Option<image::RgbaImage>,
}


impl Mesh {
    fn recalculate_tangents(&mut self) {
        // TODO: move to compute shader
        use cgmath::InnerSpace;
        let mut tangents = vec![cgmath::Vector3::new(0.0, 0.0, 0.0); self.vertices.len()];
        let mut bitangents = vec![cgmath::Vector3::new(0.0, 0.0, 0.0); self.vertices.len()];
        for idx in self.indices.chunks(3) {
            let (i1,i2,i3) = (idx[0] as usize, idx[1] as usize, idx[2] as usize);
            let p1 = cgmath::Vector3::from(self.vertices[i1].position);
            let p2 = cgmath::Vector3::from(self.vertices[i2].position);
            let p3 = cgmath::Vector3::from(self.vertices[i3].position);
            let uv1 = cgmath::Vector2::from(self.vertices[i1].texcoord);
            let uv2 = cgmath::Vector2::from(self.vertices[i2].texcoord);
            let uv3 = cgmath::Vector2::from(self.vertices[i3].texcoord);

            let (dp1, dp2) = (p2 - p1, p3 - p1);
            let (duv1, duv2) = (uv2 - uv1, uv3 - uv1);

            let determinant = duv1.x * duv2.y - duv2.x * duv1.y;
            let tangent = (duv2.y * dp1 - duv1.y * dp2) / determinant;
            let bitangent = (-duv2.x * dp1 + duv1.x * dp2) / determinant;
            tangents[i1] += tangent;
            tangents[i2] += tangent;
            tangents[i3] += tangent;
            bitangents[i1] += bitangent;
            bitangents[i2] += bitangent;
            bitangents[i3] += bitangent;
        }
        // Normalize and orthogonalize tangent vectors
        for (i,v) in self.vertices.iter_mut().enumerate() {
            let n = cgmath::Vector3::from(v.normal);
            let t = &mut tangents[i];
            let b = &bitangents[i];
            *t = (*t - t.project_on(n)).normalize();
            if b.dot(t.cross(n)) < 0.0 {
                *t *= -1.0;
            }
            v.tangent = (*t).into();
        }
    }
}


impl From<tobj::Model> for Mesh {
    fn from(model: tobj::Model) -> Self {
        let name = model.name;
        let indices = model.mesh.indices;
        let positions = model.mesh.positions
            .chunks(3)
            .map(|p| [p[0], p[1], p[2]]);
        let texcoords = model.mesh.texcoords
            .chunks(2)
            .map(|t| [t[0], 1.0 - t[1]])
            .chain(std::iter::repeat([0.0, 0.0]));
        let normals = model.mesh.normals
            .chunks(3)
            .map(|n| [n[0], n[1], n[2]])
            .chain(std::iter::repeat([0.0, 0.0, 0.0]));
        let tangents = std::iter::repeat([0.0, 0.0, 0.0]);
        let vertices = positions.zip(texcoords).zip(normals).zip(tangents)
            .map(|(((position, texcoord), normal), tangent)| {
                gpu::Vertex { position, texcoord, normal, tangent }
            })
            .collect();
        let material_id = model.mesh.material_id;
        let mut mesh = Mesh { name, vertices, indices, material_id };
        mesh.recalculate_tangents();
        mesh
    }
}


impl Material {
    pub fn from(material: tobj::Material, obj_path: &Path) -> Self {
        Material {
            name: material.name,
            ambient_color: material.ambient,
            diffuse_color: material.diffuse,
            specular_color: material.specular,
            shininess: material.shininess,
            dissolve: material.dissolve,
            optical_density: material.optical_density,
            ambient_texture: Self::load_texture(obj_path, &material.ambient_texture),
            diffuse_texture: Self::load_texture(obj_path, &material.diffuse_texture),
            specular_texture: Self::load_texture(obj_path, &material.specular_texture),
            normal_texture: Self::load_texture(obj_path, &material.normal_texture),
            dissolve_texture: None, // load_texture(obj_path, &material.dissolve_texture),
        }
    }

    fn load_texture(obj_path: &Path, texture_path: &str) -> Option<image::RgbaImage> {
        if texture_path.is_empty() {
            None
        } else {
            match image::open(obj_path.parent().unwrap().join(texture_path)) {
                Ok(img) => Some(img.into_rgba()),
                Err(err) => {
                    eprintln!("Failed to load texture '{}': {}", texture_path, err);
                    None
                },
            }
        }
    }
}


impl Model {
    pub fn from_file(path: &Path) -> Result<Self> {
        match path.extension().map(|ext| ext.to_str().unwrap()) {
            Some("obj") => Self::from_file_obj(path),
            _ => Err(anyhow!("Unknown extension in '{}'", path.to_str().unwrap())),
        }
    }

    pub fn from_file_obj(path: &Path) -> Result<Self> {
        // Read obj
        let (models, materials) = tobj::load_obj(path)?;
        // Parse meshes
        let meshes = models
            .into_iter()
            .map(Mesh::from)
            .collect();
        // Parse materials
        let materials = materials
            .into_iter()
            .map(|m| Material::from(m, path))
            // .inspect(|m| dbg!(&m))
            .collect();

        Ok(Model { meshes, materials })
    }

    pub fn extract_bounding_box(&self) -> [[f32; 3]; 2] {
        let mut bbox = [[0.0; 3]; 2];
        for d in 0..3 {
            bbox[0][d] = self.meshes.iter()
                .flat_map(|m| &m.vertices)
                .map(|v| v.position[d])
                .min_by(|c1, c2| c1.partial_cmp(c2).unwrap())
                .unwrap();
            bbox[1][d] = self.meshes.iter()
                .flat_map(|m| &m.vertices)
                .map(|v| v.position[d])
                .max_by(|c1, c2| c1.partial_cmp(c2).unwrap())
                .unwrap();
        }
        bbox
    }
}
