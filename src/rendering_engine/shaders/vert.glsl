#version 450

layout(location=0) in vec3 in_position;
layout(location=1) in vec2 in_texcoord;
layout(location=2) in vec3 in_normal;
layout(location=3) in vec3 in_tangent;

layout(location=0) out vec2 out_texcoord;
layout(location=1) out vec3 out_relative_position;
layout(location=2) out mat3 out_tangent_space_matrix;

layout(set=0, binding=0) uniform VertexShaderGlobals {
    mat4 view;
    mat4 proj;
};

void main() {
    out_texcoord = in_texcoord;

    vec4 view_position = view * vec4(in_position, 1.0);
    out_relative_position = view_position.xyz / view_position.w;
    gl_Position = proj * view_position;

    out_tangent_space_matrix = mat3(in_tangent, cross(in_tangent, in_normal), in_normal);
}