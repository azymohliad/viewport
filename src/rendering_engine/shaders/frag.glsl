#version 450

layout(location=0) in vec2 in_texcoord;
layout(location=1) in vec3 in_relative_position;
layout(location=2) in mat3 in_tangent_space_matrix;

layout(location=0) out vec4 out_color;

layout(set=1, binding=0) uniform FragmentShaderGlobals {
    vec3 light;
};

layout(set=2, binding=0) uniform texture2D diffuse_map;
layout(set=2, binding=1) uniform sampler diffuse_sampler;

layout(set=2, binding=2) uniform texture2D ambient_map;
layout(set=2, binding=3) uniform sampler ambient_sampler;

layout(set=2, binding=4) uniform texture2D specular_map;
layout(set=2, binding=5) uniform sampler specular_sampler;

layout(set=2, binding=6) uniform texture2D normal_map;
layout(set=2, binding=7) uniform sampler normal_sampler;

layout(set=2, binding=8) uniform Uniform {
    float shininess;
};


void main() {
    vec4 diffuse = texture(sampler2D(diffuse_map, diffuse_sampler), in_texcoord);
    vec4 ambient = texture(sampler2D(ambient_map, ambient_sampler), in_texcoord);
    vec4 specular = texture(sampler2D(specular_map, specular_sampler), in_texcoord);
    vec3 normal = texture(sampler2D(normal_map, normal_sampler), in_texcoord).xyz;

    vec3 light_direction = normalize(light);
    vec3 normal_direction = normalize(in_tangent_space_matrix * (normal * 2.0 - 1.0));

    float ambient_term = 0.01;
    float diffuse_term = max(dot(normal_direction, light_direction), 0.00);

    vec3 camera_direction = normalize(-in_relative_position);
    vec3 cam_light_mid_direction = normalize(light_direction + camera_direction);
    float specular_term = pow(max(dot(normal_direction, cam_light_mid_direction), 0.0), shininess);

    out_color = ambient_term * ambient + vec4(specular_term * specular.xyz, 1.0) + vec4(diffuse_term * diffuse.xyz, 1.0);
    // out_color = vec4(in_tangent_space_matrix[0] * 0.5 + 0.5, 1.0);
}

