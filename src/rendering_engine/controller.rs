use cgmath;
use winit::{
    event::{
        WindowEvent,
        KeyboardInput,
        ElementState,
        MouseButton,
        MouseScrollDelta,
        VirtualKeyCode,
    },
    dpi::PhysicalPosition,
};


pub struct CameraController {
    need_update: bool,
    // Controls state
    key_left: bool,
    key_right: bool,
    key_up: bool,
    key_down: bool,
    key_focus: bool,
    cursor_position_current: PhysicalPosition<f64>,
    cursor_position_previous: PhysicalPosition<f64>,
    mouse_scroll_delta: Option<f32>,
    mouse_orbit: bool,
    mouse_pan: bool,
    // Settings
    scroll_move_speed: f32,
    mouse_pan_speed: f32,
    mouse_rotation_speed: f32,
    key_rotation_step: cgmath::Deg<f32>,
}


impl CameraController {
    pub fn new() -> Self {
        Self {
            need_update: false,
            // Controls state
            key_left: false,
            key_right: false,
            key_up: false,
            key_down: false,
            key_focus: false,
            cursor_position_current: PhysicalPosition::new(0.0, 0.0),
            cursor_position_previous: PhysicalPosition::new(0.0, 0.0),
            mouse_scroll_delta: None,
            mouse_orbit: false,
            mouse_pan: false,
            // Settings
            scroll_move_speed: 0.01,
            mouse_pan_speed: 0.01,
            mouse_rotation_speed: 0.005,
            key_rotation_step: cgmath::Deg(15.0),
        }
    }

    pub fn process_events(&mut self, event: &WindowEvent) {
        match event {
            WindowEvent::KeyboardInput { 
                input: KeyboardInput {
                    state,
                    virtual_keycode: Some(keycode),
                    ..
                }, 
                ..
            } => {
                match keycode {
                    VirtualKeyCode::W | VirtualKeyCode::Up => {
                        self.key_up = *state == ElementState::Pressed;
                        self.need_update |= self.key_up;
                    },
                    VirtualKeyCode::A | VirtualKeyCode::Left => {
                        self.key_left = *state == ElementState::Pressed;
                        self.need_update |= self.key_left;
                    },
                    VirtualKeyCode::S | VirtualKeyCode::Down => {
                        self.key_down = *state == ElementState::Pressed;
                        self.need_update |= self.key_down;
                    },
                    VirtualKeyCode::D | VirtualKeyCode::Right => {
                        self.key_right = *state == ElementState::Pressed;
                        self.need_update |= self.key_right;
                    },
                    VirtualKeyCode::F | VirtualKeyCode::NumpadComma => {
                        self.key_focus = *state == ElementState::Pressed;
                        self.need_update |= self.key_focus;
                    },
                    _ => (), 
                }
            },
            WindowEvent::MouseInput { state, button, .. } => {
                match button {
                    MouseButton::Left => self.mouse_orbit = *state == ElementState::Pressed,
                    MouseButton::Right => self.mouse_pan = *state == ElementState::Pressed,
                    _ => (),
                }
            },
            WindowEvent::CursorMoved { position, .. } => {
                self.need_update |= self.mouse_orbit || self.mouse_pan;
                self.cursor_position_previous = self.cursor_position_current;
                self.cursor_position_current = *position;
            },
            WindowEvent::MouseWheel { delta, .. } => {
                self.mouse_scroll_delta = Some(match delta {
                    MouseScrollDelta::LineDelta(_, y) => y * 10.0,
                    MouseScrollDelta::PixelDelta(d) => d.y as f32,
                });
                self.need_update = true;

            },
            _ => (),
        }
    }


    pub fn update_camera(&mut self, camera: &mut super::scene::Camera) -> bool {
        use cgmath::{Rotation, Rotation3, Transform, InnerSpace, MetricSpace};
        
        if !self.need_update {
            return false;
        }

        let forward = (camera.target - camera.position).normalize();
        let right = forward.cross(camera.up).normalize();
        let up = right.cross(forward).normalize();

        let keys_move = self.key_left || self.key_right || self.key_up || self.key_down;

        // Orbit
        if (self.mouse_orbit && !self.mouse_pan) || keys_move {
            let (angle_horizontal, angle_vertical) = if self.mouse_orbit {
                let (x0, y0): (f32, f32) = self.cursor_position_previous.cast::<f32>().into();
                let (x1, y1): (f32, f32) = self.cursor_position_current.cast::<f32>().into();
                (cgmath::Rad((x0 - x1) * self.mouse_rotation_speed), cgmath::Rad((y0 - y1) * self.mouse_rotation_speed))
            } else {
                let mut angle_horizontal = cgmath::Deg(0.0);
                let mut angle_vertical = cgmath::Deg(0.0);
                if self.key_left { 
                    angle_horizontal += self.key_rotation_step;
                }
                if self.key_right {
                    angle_horizontal -= self.key_rotation_step;
                }
                if self.key_up { 
                    angle_vertical += self.key_rotation_step;
                }
                if self.key_down {
                    angle_vertical -= self.key_rotation_step;
                }
                (angle_horizontal.into(), angle_vertical.into())
            };
            let quat1 = cgmath::Quaternion::from_axis_angle(up, angle_horizontal);
            let quat2 = cgmath::Quaternion::from_axis_angle(right, angle_vertical);
            camera.position = (quat1 * quat2).rotate_point(camera.position);
        }

        // Pan
        if self.mouse_pan && !self.mouse_orbit {
            let (x0, y0): (f32, f32) = self.cursor_position_previous.cast::<f32>().into();
            let (x1, y1): (f32, f32) = self.cursor_position_current.cast::<f32>().into();
            let pan_horizontal = (x0 - x1) * self.mouse_pan_speed; 
            let pan_vertical = (y1 - y0) * self.mouse_pan_speed; 
            let pan_vector = pan_vertical * up + pan_horizontal * right;
            let tranform = cgmath::Matrix4::from_translation(pan_vector);
            camera.position = tranform.transform_point(camera.position);
            camera.target = tranform.transform_point(camera.target);
        }

        // Move forward & backward
        if let Some(scroll_delta) = self.mouse_scroll_delta.take() {
            camera.position += forward * scroll_delta * self.scroll_move_speed * camera.position.distance(camera.target);
        }

        // Focus
        if self.key_focus {
            camera.target = cgmath::Point3::new(0.0, 0.0, 0.0);
        }
        
        self.need_update = keys_move;
        true
    }
}
