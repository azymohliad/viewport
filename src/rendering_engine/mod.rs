
mod gpu;
mod scene;
mod controller;
mod model;

use std::path::Path;
use raw_window_handle::HasRawWindowHandle;
use anyhow::Result;
use winit::event::WindowEvent;


pub struct RenderingEngine {
    gpu_context: gpu::Context,
    light: scene::Light, 
    camera: scene::Camera,
    camera_controller: controller::CameraController,
    model: Option<gpu::ModelHandle>,
}

impl RenderingEngine {
    pub fn new<W: HasRawWindowHandle>(window: &W, width: u32, height: u32) -> Result<Self> {
        let mut rendering_engine = Self {
            gpu_context: gpu::Context::new(window, width, height)?,
            light: scene::Light {
                position: cgmath::Point3::new(1.0, 1.0, 1.0),
            },
            camera: scene::Camera {
                position: cgmath::Point3::new(0.0, 0.5, 1.0),
                target: cgmath::Point3::new(0.0, 0.0, 0.0),
                up: cgmath::Vector3::unit_y(),
                aspect: width as f32 / height as f32,
                fov: 45.0,
                z_near: 0.01,
                z_far: 100.0
            },
            camera_controller: controller::CameraController::new(),
            model: None,
        };
        rendering_engine.update_vertex_shader_globals();
        rendering_engine.update_fragment_shader_globals();
        Ok(rendering_engine)
    }

    pub fn process_events(&mut self, event: &WindowEvent) {
        self.camera_controller.process_events(event);
    }

    pub fn update(&mut self) {
        if self.camera_controller.update_camera(&mut self.camera) {
            self.update_vertex_shader_globals();
        }
    }

    pub fn load_model_from_file(&mut self, path: &Path) -> Result<()> {
        use cgmath::{Angle,EuclideanSpace};

        let model_data = model::Model::from_file(path)?;
        self.model = Some(self.gpu_context.load_model(&model_data));

        // Update camera
        let bbox = model_data.extract_bounding_box();
        let midpoint = cgmath::Point3::from(bbox[0]).midpoint(cgmath::Point3::from(bbox[1]));
        let width = bbox[1][0] - bbox[0][0];
        let height = bbox[1][1] - bbox[0][1];
        let tan = cgmath::Deg(self.camera.fov / 2.0).tan();
        let distance_by_height = 0.7 * height / tan;
        let distance_by_width = 0.7 * width / (tan * self.camera.aspect);
        let distance = distance_by_height.max(distance_by_width);
        self.camera.position = cgmath::Point3::new(0.0, midpoint[1], bbox[1][2] + distance);
        self.camera.target = midpoint;
        self.update_vertex_shader_globals();

        Ok(())
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.gpu_context.resize(width, height);
        self.camera.aspect = width as f32 / height as f32;
        self.update_vertex_shader_globals();
    }

    pub fn render(&mut self) {
        self.gpu_context.render(self.model.as_ref());
    }

    fn update_vertex_shader_globals(&mut self) {
        self.gpu_context.update_vertex_shader_globals(gpu::VertexShaderGlobals {
            view: self.camera.build_view_matrix(),
            proj: self.camera.build_projection_matrix(),
        });
    }

    fn update_fragment_shader_globals(&mut self) {
        self.gpu_context.update_fragment_shader_globals(gpu::FragmentShaderGlobals {
            light: self.light.position,
        });
    }
}