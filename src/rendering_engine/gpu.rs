use std::fs::File;
use raw_window_handle::HasRawWindowHandle;
use anyhow::{Result, anyhow};
use futures::executor::block_on;
use bytemuck::{Zeroable, Pod};

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub position: [f32;3],
    pub texcoord: [f32;2],
    pub normal: [f32;3],
    pub tangent: [f32;3],
}
unsafe impl Zeroable for Vertex {}
unsafe impl Pod for Vertex {}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct VertexShaderGlobals {
    pub view: cgmath::Matrix4<f32>,
    pub proj: cgmath::Matrix4<f32>,
}
unsafe impl Zeroable for VertexShaderGlobals {}
unsafe impl Pod for VertexShaderGlobals {}


#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct FragmentShaderGlobals {
    pub light: cgmath::Point3<f32>,
}
unsafe impl Zeroable for FragmentShaderGlobals {}
unsafe impl Pod for FragmentShaderGlobals {}

pub struct ModelHandle {
    meshes: Vec<MeshHandle>,
    materials: Vec<MaterialHandle>,
}


pub struct MeshHandle {
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    index_count: u32,
    material_id: Option<usize>
}


pub struct MaterialHandle {
    bind_group: wgpu::BindGroup,
}

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct MaterialUniform {
    shininess: f32,
}
unsafe impl Zeroable for MaterialUniform {}
unsafe impl Pod for MaterialUniform {}


pub struct TextureHandle {
    texture: wgpu::Texture,
    view: wgpu::TextureView,
    sampler: wgpu::Sampler,
}

pub struct Context {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    swap_chain_descriptor: wgpu::SwapChainDescriptor,
    swap_chain: wgpu::SwapChain,
    render_pipeline: wgpu::RenderPipeline,
    depth_view: wgpu::TextureView,
    material_bind_group_layout: wgpu::BindGroupLayout,
    fallback_material_bind_group: wgpu::BindGroup,
    vs_globals_bind_group: wgpu::BindGroup,
    vs_globals_buffer: wgpu::Buffer,
    fs_globals_bind_group: wgpu::BindGroup,
    fs_globals_buffer: wgpu::Buffer,
}

impl Context {
    pub fn new<W: HasRawWindowHandle>(window: &W, width: u32, height: u32) -> Result<Self> {
        // Create surface
        let surface = wgpu::Surface::create(window);

        // Select GPU
        let adapter = block_on(wgpu::Adapter::request(
                &wgpu::RequestAdapterOptions {
                    power_preference: wgpu::PowerPreference::Default,
                    compatible_surface: Some(&surface),
                },
                wgpu::BackendBit::PRIMARY))
            .ok_or(anyhow!("Suitable video adapter not found"))?;
        println!("{:?}", adapter.get_info());

        // Get device and command queue
        let (device, queue) = block_on(adapter.request_device(&wgpu::DeviceDescriptor {
                extensions: wgpu::Extensions {
                    anisotropic_filtering: false,
                },
                limits: wgpu::Limits { max_bind_groups: 5 },
            }));

        // Create swap chain
        let swap_chain_descriptor = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8UnormSrgb,
            width: width,
            height: height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &swap_chain_descriptor);

        // Load shaders
        // let vs_src = include_str!("shaders/vert.glsl");
        // let vs_spv = glsl_to_spirv::compile(vs_src, glsl_to_spirv::ShaderType::Vertex).map_err(|m| anyhow!(m))?;
        // let vs_data = wgpu::read_spirv(vs_spv)?;
        let vs_data = wgpu::read_spirv(File::open(env!("VIEWPORT_VERTEX_SHADER_PATH"))?)?;
        let vs_module = device.create_shader_module(&vs_data);

        // let fs_src = include_str!("shaders/frag.glsl");
        // let fs_spv = glsl_to_spirv::compile(fs_src, glsl_to_spirv::ShaderType::Fragment).map_err(|m| anyhow!(m))?;
        // let fs_data = wgpu::read_spirv(fs_spv)?;
        let fs_data = wgpu::read_spirv(File::open(env!("VIEWPORT_FRAGMENT_SHADER_PATH"))?)?;
        let fs_module = device.create_shader_module(&fs_data);

        // Define buffers and uniforms layouts
        // -- Vertex shader globals
        let vs_globals_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            bindings: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::VERTEX,
                    ty: wgpu::BindingType::UniformBuffer { dynamic: false }
                }
            ],
        });

        let vs_globals_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: std::mem::size_of::<VertexShaderGlobals>() as wgpu::BufferAddress,
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        });

        let vs_globals_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &vs_globals_bind_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer {
                        buffer: &vs_globals_buffer,
                        range: 0..std::mem::size_of::<VertexShaderGlobals>() as wgpu::BufferAddress
                    }
                }
            ]
        });

        // -- Fragment shader globals
        let fs_globals_bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            bindings: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::UniformBuffer { dynamic: false }
                }
            ]
        });

        let fs_globals_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: std::mem::size_of::<FragmentShaderGlobals>() as wgpu::BufferAddress,
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        });

        let fs_globals_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &fs_globals_bind_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer {
                        buffer: &fs_globals_buffer,
                        range: 0..std::mem::size_of::<FragmentShaderGlobals>() as wgpu::BufferAddress
                    }
                }
            ],
        });

        // -- Textures
       let material_bind_group_layout = MaterialHandle::bind_group_layout(&device);

        // -- Vertex buffer
        let vertex_buffer_descriptor = wgpu::VertexBufferDescriptor {
            stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttributeDescriptor {
                    offset: 0,
                    format: wgpu::VertexFormat::Float3,
                    shader_location: 0,
                },
                wgpu::VertexAttributeDescriptor {
                    offset: std::mem::size_of::<[f32;3]>() as wgpu::BufferAddress,
                    format: wgpu::VertexFormat::Float2,
                    shader_location: 1,
                },
                wgpu::VertexAttributeDescriptor {
                    offset: (std::mem::size_of::<[f32;3]>() + std::mem::size_of::<[f32;2]>()) as wgpu::BufferAddress,
                    format: wgpu::VertexFormat::Float3,
                    shader_location: 2,
                },
                wgpu::VertexAttributeDescriptor {
                    offset: (std::mem::size_of::<[f32;3]>() + std::mem::size_of::<[f32;2]>() + std::mem::size_of::<[f32;3]>()) as wgpu::BufferAddress,
                    format: wgpu::VertexFormat::Float3,
                    shader_location: 3,
                }
            ],
        };

        // -- Depth buffer
        let depth_texture_descriptor = wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d { width: width, height: height, depth: 1 },
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth32Float,
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        };
        let depth_texture = device.create_texture(&depth_texture_descriptor);
        let depth_view = depth_texture.create_default_view();

        // -- Fallback material
        let fallback_material_data = super::model::Material {
            name: String::from("Fallback"),
            ambient_color: [1.0, 1.0, 1.0],
            diffuse_color: [0.8, 0.8, 0.8],
            specular_color: [0.8, 0.8, 0.8],
            shininess: 128.0,
            dissolve: 0.0,
            optical_density: 0.0,
            ambient_texture: None,
            diffuse_texture: None,
            specular_texture: None,
            normal_texture: None,
            dissolve_texture: None,
        };
        let (fallback_material, load_cmds) = MaterialHandle::load(&device, &material_bind_group_layout, &fallback_material_data);
        let fallback_material_bind_group = fallback_material.bind_group;
        queue.submit(&load_cmds);

        // Create render pipeline
        let render_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[
                &vs_globals_bind_group_layout,
                &fs_globals_bind_group_layout,
                &material_bind_group_layout,
            ],
        });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: &render_pipeline_layout,
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: Some(wgpu::RasterizationStateDescriptor {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: wgpu::CullMode::None,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
            }),
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[
                wgpu::ColorStateDescriptor {
                    format: swap_chain_descriptor.format,
                    color_blend: wgpu::BlendDescriptor::REPLACE,
                    alpha_blend: wgpu::BlendDescriptor::REPLACE,
                    write_mask: wgpu::ColorWrite::ALL,
                },
            ],
            depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
                format: wgpu::TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil_front: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_back: wgpu::StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask: 0,
                stencil_write_mask: 0,
            }),
            vertex_state: wgpu::VertexStateDescriptor {
                vertex_buffers: &[vertex_buffer_descriptor],
                index_format: wgpu::IndexFormat::Uint32,
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

        // Create Context object
        Ok(Self {
            surface,
            device,
            queue,
            swap_chain_descriptor,
            swap_chain,
            render_pipeline,
            depth_view,
            material_bind_group_layout,
            fallback_material_bind_group,
            vs_globals_bind_group,
            vs_globals_buffer,
            fs_globals_bind_group,
            fs_globals_buffer,
        })
    }


    pub fn update_vertex_shader_globals(&self, vs_globals: VertexShaderGlobals) {
        let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
        let staging_buffer = self.device.create_buffer_with_data(bytemuck::bytes_of(&vs_globals), wgpu::BufferUsage::COPY_SRC);
        encoder.copy_buffer_to_buffer(&staging_buffer, 0, &self.vs_globals_buffer, 0, std::mem::size_of::<VertexShaderGlobals>() as wgpu::BufferAddress);
        self.queue.submit(&[encoder.finish()]);
    }


    pub fn update_fragment_shader_globals(&self, fs_globals: FragmentShaderGlobals) {
        let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
        let staging_buffer = self.device.create_buffer_with_data(bytemuck::bytes_of(&fs_globals), wgpu::BufferUsage::COPY_SRC);
        encoder.copy_buffer_to_buffer(&staging_buffer, 0, &self.fs_globals_buffer, 0, std::mem::size_of::<FragmentShaderGlobals>() as wgpu::BufferAddress);
        self.queue.submit(&[encoder.finish()]);
    }


    pub fn load_model(&self, data: &super::model::Model) -> ModelHandle {
        let (model, commands) = ModelHandle::load(&self.device, &self.material_bind_group_layout, data);
        self.queue.submit(&commands);
        model
    }


    pub fn resize(&mut self, width: u32, height: u32) {
        self.swap_chain_descriptor.height = height;
        self.swap_chain_descriptor.width = width;
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.swap_chain_descriptor);

        let depth_texture = self.device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d { width: width, height: height, depth: 1 },
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth32Float,
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        });
        self.depth_view = depth_texture.create_default_view();
    }


    pub fn render(&mut self, model: Option<&ModelHandle>) {
        if let Ok(frame) = self.swap_chain.get_next_texture() {
            let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
            {
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    color_attachments: &[
                        wgpu::RenderPassColorAttachmentDescriptor {
                            attachment: &frame.view,
                            resolve_target: None,
                            load_op: wgpu::LoadOp::Clear,
                            store_op: wgpu::StoreOp::Store,
                            clear_color: wgpu::Color { r: 0.1, g: 0.2, b: 0.3, a: 1.0 }
                        }
                    ],
                    depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachmentDescriptor {
                        attachment: &self.depth_view,
                        depth_load_op: wgpu::LoadOp::Clear,
                        depth_store_op: wgpu::StoreOp::Store,
                        clear_depth: 1.0,
                        stencil_load_op: wgpu::LoadOp::Clear,
                        stencil_store_op: wgpu::StoreOp::Store,
                        clear_stencil: 0
                    })
                });

                render_pass.set_pipeline(&self.render_pipeline);
                render_pass.set_bind_group(0, &self.vs_globals_bind_group, &[]);
                render_pass.set_bind_group(1, &self.fs_globals_bind_group, &[]);
                if let Some(ref model) = model {
                    for mesh in model.meshes.iter() {
                        let material_bind_group = match mesh.material_id.map(|i| &model.materials[i]) {
                            Some(ref material) => &material.bind_group,
                            _ => &self.fallback_material_bind_group,
                        };
                        render_pass.set_bind_group(2, material_bind_group, &[]);
                        render_pass.set_vertex_buffer(0, &mesh.vertex_buffer, 0, 0);
                        render_pass.set_index_buffer(&mesh.index_buffer, 0, 0);
                        render_pass.draw_indexed(0..mesh.index_count, 0, 0..1);
                    }
                }
            }
            self.queue.submit(&[encoder.finish()]);
        }
    }
}


impl ModelHandle {
    fn load(device: &wgpu::Device, material_layout: &wgpu::BindGroupLayout, data: &super::model::Model) -> (ModelHandle, Vec<wgpu::CommandBuffer>) {
        let meshes = data.meshes
            .iter()
            .map(|mesh| MeshHandle::load(device, mesh))
            .collect();
        let (materials, commands): (Vec<_>, Vec<Vec<_>>) = data.materials
            .iter()
            .map(|mat| MaterialHandle::load(device, material_layout, mat))
            .unzip();
        let commands = commands.into_iter().flatten().collect();

        (ModelHandle { meshes, materials }, commands)
    }
}


impl MeshHandle {
    fn load(device: &wgpu::Device, data: &super::model::Mesh) -> MeshHandle {
        let vertex_buffer = device.create_buffer_with_data(bytemuck::cast_slice(&data.vertices), wgpu::BufferUsage::VERTEX);
        let index_buffer = device.create_buffer_with_data(bytemuck::cast_slice(&data.indices), wgpu::BufferUsage::INDEX);
        let index_count = data.indices.len() as u32;
        let material_id = data.material_id;
        MeshHandle { vertex_buffer, index_buffer, index_count, material_id }
    }
}


impl MaterialHandle {
    fn load(device: &wgpu::Device, layout: &wgpu::BindGroupLayout, data: &super::model::Material) -> (MaterialHandle, Vec<wgpu::CommandBuffer>) {
        let (diffuse, diffuse_cmd) = TextureHandle::load_image_or_rgb_color(device, data.diffuse_texture.as_ref(), data.diffuse_color);
        let (ambient, ambient_cmd) = TextureHandle::load_image_or_rgb_color(device, data.ambient_texture.as_ref(), data.ambient_color);
        let (specular, specular_cmd) = TextureHandle::load_image_or_rgb_color(device, data.specular_texture.as_ref(), data.specular_color);
        let (normal, normal_cmd) = TextureHandle::load_image_or_rgb_color(device, data.normal_texture.as_ref(), [0.0, 0.0, 1.0]);

        let uniform = MaterialUniform {
            shininess: data.shininess,
        };

        let uniform_buffer = device.create_buffer_with_data(bytemuck::bytes_of(&uniform), wgpu::BufferUsage::UNIFORM);

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout,
            bindings: &[
                wgpu::Binding {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&diffuse.view),
                },
                wgpu::Binding {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&diffuse.sampler),
                },
                wgpu::Binding {
                    binding: 2,
                    resource: wgpu::BindingResource::TextureView(&ambient.view),
                },
                wgpu::Binding {
                    binding: 3,
                    resource: wgpu::BindingResource::Sampler(&ambient.sampler),
                },
                wgpu::Binding {
                    binding: 4,
                    resource: wgpu::BindingResource::TextureView(&specular.view),
                },
                wgpu::Binding {
                    binding: 5,
                    resource: wgpu::BindingResource::Sampler(&specular.sampler),
                },
                wgpu::Binding {
                    binding: 6,
                    resource: wgpu::BindingResource::TextureView(&normal.view),
                },
                wgpu::Binding {
                    binding: 7,
                    resource: wgpu::BindingResource::Sampler(&normal.sampler),
                },
                wgpu::Binding {
                    binding: 8,
                    resource: wgpu::BindingResource::Buffer {
                        buffer: &uniform_buffer,
                        range: 0..std::mem::size_of::<MaterialUniform>() as wgpu::BufferAddress
                    }
                },
            ],
        });

        (MaterialHandle { bind_group }, vec![diffuse_cmd, ambient_cmd, specular_cmd, normal_cmd])
    }

    fn bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            bindings: &[
                // Diffuse texture
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        dimension: wgpu::TextureViewDimension::D2,
                        component_type: wgpu::TextureComponentType::Float,
                        multisampled: false,
                    },
                },
                // Diffuse sampler
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                },
                // Ambient texture
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        dimension: wgpu::TextureViewDimension::D2,
                        component_type: wgpu::TextureComponentType::Float,
                        multisampled: false,
                    },
                },
                // Ambient sampler
                wgpu::BindGroupLayoutEntry {
                    binding: 3,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                },
                // Specular texture
                wgpu::BindGroupLayoutEntry {
                    binding: 4,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        dimension: wgpu::TextureViewDimension::D2,
                        component_type: wgpu::TextureComponentType::Float,
                        multisampled: false,
                    },
                },
                // Specular sampler
                wgpu::BindGroupLayoutEntry {
                    binding: 5,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                },
                // Normal texture
                wgpu::BindGroupLayoutEntry {
                    binding: 6,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        dimension: wgpu::TextureViewDimension::D2,
                        component_type: wgpu::TextureComponentType::Float,
                        multisampled: false,
                    },
                },
                // Normal sampler
                wgpu::BindGroupLayoutEntry {
                    binding: 7,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                },
                // Other data
                wgpu::BindGroupLayoutEntry {
                    binding: 8,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::UniformBuffer { dynamic: false },
                },
            ],
        })
    }
}


impl TextureHandle {
    fn load(device: &wgpu::Device, img: &image::RgbaImage) -> (TextureHandle, wgpu::CommandBuffer) {
        // Create texture object
        let img_size = img.dimensions();

        let size = wgpu::Extent3d {
            width: img_size.0,
            height: img_size.1,
            depth: 1,
        };

        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: None,
            size,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
        });

        let view = texture.create_default_view();

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::Repeat,
            address_mode_v: wgpu::AddressMode::Repeat,
            address_mode_w: wgpu::AddressMode::Repeat,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: wgpu::CompareFunction::Always,
        });

        // Create command buffer to copy texture to GPU memory
        let staging_buffer = device.create_buffer_with_data(&img, wgpu::BufferUsage::COPY_SRC);

        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        encoder.copy_buffer_to_texture(
            wgpu::BufferCopyView {
                buffer: &staging_buffer,
                offset: 0,
                bytes_per_row: std::mem::size_of::<image::Rgba<u8>>() as u32 * img_size.0,
                rows_per_image: img_size.1,
            },
            wgpu::TextureCopyView {
                texture: &texture,
                mip_level: 0,
                array_layer: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            size,
        );

        let texture_handle = TextureHandle { texture, view, sampler };
        let command_buffer = encoder.finish();
        (texture_handle, command_buffer)
    }


    fn load_rgb_color(device: &wgpu::Device, rgb: [f32;3]) -> (TextureHandle, wgpu::CommandBuffer) {
        let rgba = image::Rgba([(rgb[0] * 255.0) as u8, (rgb[1] * 255.0) as u8, (rgb[2] * 255.0) as u8, 255]);
        Self::load(device, &image::RgbaImage::from_fn(1, 1, |_,_| rgba))
    }


    fn load_image_or_rgb_color(device: &wgpu::Device, image: Option<&image::RgbaImage>, rgb: [f32;3]) -> (TextureHandle, wgpu::CommandBuffer) {
        match image {
            Some(img) => Self::load(device, img),
            None => Self::load_rgb_color(device, rgb)
        }
    }
}