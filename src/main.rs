mod rendering_engine;

use std::path::Path;
use std::env;
use winit::{
    event::{
        Event,
        WindowEvent,
        KeyboardInput,
        ElementState,
        VirtualKeyCode
    },
    window::Window,
    event_loop::{EventLoop, ControlFlow},
};


fn main() {
    // Initialize event loop and main window
    let event_loop = EventLoop::new();
    let main_window = Window::new(&event_loop)
        .expect("Failed to initialize main window");
    main_window.set_title("Viewport");
    let (width, height) = main_window.inner_size().into();

    // Initialize rendering engine
    let mut rendering_engine = rendering_engine::RenderingEngine::new(&main_window, width, height)
        .expect("Failed to initialize rendering engine");

    // Load model
    let args = env::args().collect::<Vec<_>>();
    if args.len() == 2 {
        rendering_engine.load_model_from_file(Path::new(&args[1]))
            .expect("Failed to load model");
    }

    // Run event loop
    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::WindowEvent { ref event, window_id } if window_id == main_window.id() => {
                match event {
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    WindowEvent::Resized(size) => {
                        rendering_engine.resize(size.width, size.height);
                        *control_flow = ControlFlow::Wait;
                    },
                    WindowEvent::DroppedFile(path) => {
                        rendering_engine.load_model_from_file(&path)
                            .expect("Failed to load model");
                        *control_flow = ControlFlow::Wait;
                    },
                    _ => {
                        rendering_engine.process_events(event);
                        *control_flow = ControlFlow::Wait;
                    },
                }
            },
            Event::MainEventsCleared => {
                rendering_engine.update();
                rendering_engine.render();
                *control_flow = ControlFlow::Wait;
            },
            _ => *control_flow = ControlFlow::Wait,
        }
    });
}
