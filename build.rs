use std::env;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;



fn compile_shader(src_path: &str, out_path: &str, shader_type: glsl_to_spirv::ShaderType) -> Result<(),String>
{
    println!("cargo:rerun-if-changed={}", src_path);
    let mut src_file = File::open(src_path).map_err(|e| format!("{}", e))?;
    let mut src = String::new();
    src_file.read_to_string(&mut src).map_err(|e| format!("{}", e))?;
    let mut spirv_file = glsl_to_spirv::compile(&src, shader_type)?;
    let mut spirv = Vec::new();
    spirv_file.read_to_end(&mut spirv).map_err(|e| format!("{}", e))?;
    File::create(&out_path).map_err(|e| format!("{}", e))?
        .write_all(&spirv).map_err(|e| format!("{}", e))?;
    Ok(())
}


fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let out_dir = Path::new(&out_dir);

    let vs_src_path = "src/rendering_engine/shaders/vert.glsl";
    let vs_out_path = out_dir.join("vert.spv");
    let vs_out_path = vs_out_path.to_str().unwrap();
    compile_shader(vs_src_path, vs_out_path, glsl_to_spirv::ShaderType::Vertex)
        .expect("Failed to compile vertex shader");
    println!("cargo:rustc-env={}={}", "VIEWPORT_VERTEX_SHADER_PATH", vs_out_path);
    
    let fs_src_path = "src/rendering_engine/shaders/frag.glsl";
    let fs_out_path = out_dir.join("frag.spv");
    let fs_out_path = fs_out_path.to_str().unwrap();
    compile_shader(fs_src_path, fs_out_path, glsl_to_spirv::ShaderType::Fragment)
        .expect("Failed to compile fragment shader");
    println!("cargo:rustc-env={}={}", "VIEWPORT_FRAGMENT_SHADER_PATH", fs_out_path);
}
